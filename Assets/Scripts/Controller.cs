﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	private Character player;
	public float speed;
	public float jumpForce;


	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Character>();
		jumpForce *= 30f;
	}
	
	// Update is called once per frame
	void Update () {
		HorizontalMovement ();
		Jump ();
		Attack ();
	}


	void HorizontalMovement()
	{
		if(Input.GetButton("Horizontal"))
		{
			if (Input.GetAxis ("Horizontal") > 0) {
				player.transform.position += Vector3.right * speed * Time.deltaTime;
				player.transform.position = new Vector3 (player.transform.position.x,player.transform.position.y,0f);

				//mirar si hay que cambiar donde mira personaje
				if(!player.facingRight)
				{
					Flip ();
					player.facingRight = true;
				}
			} 
			else 
			{
				player.transform.position += Vector3.left * speed * Time.deltaTime;
				player.transform.position = new Vector3 (player.transform.position.x,player.transform.position.y,0f);

				//mirar si hay que cambiar donde mira personaje
				if(player.facingRight)
				{
					Flip ();
					player.facingRight = false;
				}
			}
		}
	}

	//salto
	private void Jump()
	{
		if (Input.GetButtonDown ("Jump") && player.isGrounded) 
		{
			player.rb.AddForce (0f,jumpForce,0f);
		}
	}

	private void Flip()
	{
		// Multiply the player's x local scale by -1.
		Vector3 theScale = player.transform.localScale;
		theScale.x *= -1;
		player.transform.localScale = theScale;
	}

	//atacar
	private void Attack()
	{
		if(Input.GetButtonDown("Fire1"))
		{


			//character conoce que arma lleva, desde variable player accedemos a las funciones del arma

			//mirar si esa arma es del tipo pistola
			if(player.currentWeapon.weaponType == Weapon.Weapons.Gun)
			{
				if (Input.GetAxis ("Vertical") > 0) {
					player.currentWeapon.ShootBullet ("up");
				} 
				else 
				{
					if (player.facingRight) {
						player.currentWeapon.ShootBullet ("right");
					} 
					else 
					{
						player.currentWeapon.ShootBullet ("left");
					}
				}
			}

			//SOLO ESTA HECHO EL ATAQUE CON PISTOLA


		}
	}

	//cambiar de arma
	private void ChangeWeapon()
	{
		
	}




		
}
