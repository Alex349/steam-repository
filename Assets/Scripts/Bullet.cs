﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	//disparos
	public float gunBulletSpeed;
	[HideInInspector]public string gunDirection;

	// Use this for initialization
	void Start () {
		Flip ();
	}
	
	// Update is called once per frame
	void Update () {
		Movement ();
	}



	void Movement()
	{
		switch (gunDirection) 
		{
			case "up":
				this.transform.position += Vector3.up * gunBulletSpeed * Time.deltaTime;
				break;
			case "right":
				this.transform.position += Vector3.right * gunBulletSpeed * Time.deltaTime;
				break;
			case "left":
				this.transform.position += Vector3.left * gunBulletSpeed * Time.deltaTime;
				break;
		}

	}


	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Enemy")
		{
			//apply damage, destroy
		}
		else if(other.tag == "Obstacle")
		{
			//destroy
			Destroy(gameObject);
		}
	}

	void Flip()
	{
		if(gunDirection == "left")
		{
			// Multiply the player's x local scale by -1.
			Vector3 theScale = this.transform.localScale;
			theScale.x *= -1;
			this.transform.localScale = theScale;
		}
		else if(gunDirection == "up")
		{
			this.transform.localEulerAngles += new Vector3 (0f,0f,90f);
		}

	}
}
