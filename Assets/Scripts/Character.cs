﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

	//vida
	public int hp;
	[HideInInspector]public bool isGrounded;

	[HideInInspector]public Rigidbody rb;
	private SpriteRenderer sr;
	public bool facingRight; //donde mira personaje
	public Weapon currentWeapon;//tiene una arma

	// Use this for initialization
	void Start () {
		this.tag = "Player";
		hp = 0;
		rb = GetComponent<Rigidbody> ();
		sr = GetComponent<SpriteRenderer> ();
		facingRight = true;
		currentWeapon = GetComponent<Weapon> ();

		//probando, ponemos tipo pistola como arma basica
		currentWeapon.weaponType = Weapon.Weapons.Gun;
	}
	
	// Update is called once per frame
	void Update () {
		isGrounded = TouchFloor ();//si el raycast toca objeto con tag floor
		this.transform.rotation = Quaternion.identity;//personaje no puede rotar
	}

	//raycast de si el personaje toca suelo
	private bool TouchFloor()
	{
		//donde empieza el ray
		Vector3 leftRayStart = this.transform.position;
		Vector3 rightRayStart = this.transform.position;


		//se suma lo que ocupa el sprite en el eje X para ver los extremos
		leftRayStart.x -= sr.bounds.extents.x;
		rightRayStart.x += sr.bounds.extents.x;

		//pintado en escena
		Debug.DrawRay (leftRayStart,Vector3.down,Color.red);
		Debug.DrawRay (rightRayStart,Vector3.down,Color.blue);
	
		RaycastHit hit;//donde golpea el rayo
		//inicioRay, forward del rayo, donde golpea, longitud del rayo
		if (Physics.Raycast (leftRayStart, Vector3.down, out hit, sr.bounds.extents.y + 0.1f) || Physics.Raycast (rightRayStart, Vector3.down, out hit, sr.bounds.extents.y + 0.1f)) 
		{
			if(hit.transform.tag == "Floor")
			{
				return true;
			}
		}
		return false;

	}//final TouchFloor


		

}
