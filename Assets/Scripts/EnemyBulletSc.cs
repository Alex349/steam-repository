﻿using UnityEngine;
using System.Collections;

public class EnemyBulletSc : MonoBehaviour {

	public float speed = 5f;
	public Vector2 _direction;
	public bool isReady = false;


	// Use this for initialization
	void Start () {
	
	}

	public void setDirection (Vector2 direction){
		_direction = direction.normalized;
		isReady = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(isReady){
			Vector2 position = transform.position;
			position += _direction * speed * Time.deltaTime;
			transform.position = position;
			Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
			Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
			if ((transform.position.x < min.x) || (transform.position.x > max.x) ||
				(transform.position.y < min.y) || (transform.position.y > min.y))
				{
				Destroy (gameObject);
				}

		}
	
	}
}
