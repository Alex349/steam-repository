﻿using UnityEngine;
using System.Collections;

public class GameManager{

	public static GameManager instance;

	public static GameManager GetInstance()
	{
		if(instance == null)
		{
			instance = new GameManager ();
		}
		return instance;
	}
}
