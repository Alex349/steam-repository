﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public int damage;

	public enum Weapons {Axe, Sword, Gun,RayCaster};
	Character player;//no va aqui (quizas)
	public Weapons weaponType;//variable del enum

	// Use this for initialization
	void Start () {
		player = GetComponent<Character> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//ESTO ES DE PRUEBA
	public void ShootBullet(string direction)
	{
		GameObject bullet = (GameObject)Instantiate (Resources.Load("Bullet"),player.transform.position,Quaternion.identity);
		bullet.GetComponent<Bullet>().gunDirection = direction; //hacia donde va la bala
	}
		
}
