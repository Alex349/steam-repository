﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private Vector2 velocity;
	public float smoothY;
	public float smoothX;
	public GameObject Player;
	public bool Bounds;
	public Vector3 MaxCameraBounds;
	public Vector3 MinCameraBounds;



	// Use this for initialization
	void Start () {
		Player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float posX = Mathf.SmoothDamp (transform.position.x, Player.transform.position.x, ref velocity.x, smoothX);
		float posY = Mathf.SmoothDamp (transform.position.y, Player.transform.position.y, ref velocity.y, smoothY);

		transform.position = new Vector3 (posX, posY, transform.position.z);

		if (Bounds) {
			transform.position = new Vector3 (Mathf.Clamp (transform.position.x, MinCameraBounds.x, MaxCameraBounds.x), 
				Mathf.Clamp (transform.position.y, MinCameraBounds.y, MaxCameraBounds.y),
				Mathf.Clamp (transform.position.z, MinCameraBounds.z, MaxCameraBounds.z));


		}
	}
}
